# [C9x3] Lets Make Getting Firefox Easy

Try to redirect Mozilla URLs to just the download site for Firefox.

# Licensing and donation information:

https://c9x3.neocities.org/

# How to run this Userscript:

- Get a userscript runner. For examlpe, Violentmonkey. 
- Create a new script and copy and paste the contents into the new userscript! 
- Save that script, exit and test the script out. 
