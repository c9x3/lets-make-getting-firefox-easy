// ==UserScript==
// @name               [C9x3] Lets Make Getting Firefox Easy.
// @description        Try to redirect Mozilla URLs to just the download site for Firefox.
//                     You CAN disable parts of this UserScript by marking the parts you want disabled as a comment!
// @match               *://*/*
// @version            0.8
// @run-at             document-start
// @grant              none
// ProjectHomepage      https://c9x3.neocities.org/
// @downloadURL  		https://gitlab.com/c9x3/lets-make-getting-firefox-easy/-/raw/main/Lets_Make_Getting_Firefox_Easy.js
// @updateURL    		https://gitlab.com/c9x3/lets-make-getting-firefox-easy/-/raw/main/Lets_Make_Getting_Firefox_Easy.js
// ==/UserScript==

//

// Make a log in the console when the script starts. 

console.log('Lets make getting Firefox easy has started!')

//

// Don’t run in frames.

if (window.top !== window.self)	  

return;

var currentURL = location.href;

//

// Try to replace, "www.mozilla.org/en-US/firefox/download/thanks/" with, "www.mozilla.org/en-US/firefox/all/#product-desktop-release."

if (currentURL.match("www.mozilla.org/en-US/firefox/download/thanks/")) {
  
	location.href = location.href.replace("www.mozilla.org/en-US/firefox/download/thanks/", "www.mozilla.org/en-US/firefox/all/#product-desktop-release");
  
};

//

// Try to replace, "www.mozilla.org/en-US/firefox/new/" with, "www.mozilla.org/en-US/firefox/all/#product-desktop-release."

if (currentURL.match("www.mozilla.org/en-US/firefox/new/")) {
  
	location.href = location.href.replace("www.mozilla.org/en-US/firefox/new/", "www.mozilla.org/en-US/firefox/all/#product-desktop-release");
  
};

//

